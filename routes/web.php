<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\Route;



Route::get('/lista_de_compras', 'ListaDeComprasController@index')->name('lista_de_compras');
Route::post('/importacaodados', 'ListaDeComprasController@importaDados')->name('importacaodados');



Route::get('/inclusao', 'ResetController@inclusao')->name('inclusao');
Route::post('/add_instituicao', 'ResetController@add_instituicao')->name('add_instituicao');
Route::post("add_anexo", "ResetController@add_anexo")->name('add_anexo'); 
Route::post("edit_inst", "ResetController@edit_inst")->name('edit_inst'); 




Route::namespace('Auth')->group(function(){ 
    Route::get("login", "LogarController@login")->name('login');  //rota de login


});
