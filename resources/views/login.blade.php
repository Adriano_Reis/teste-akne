<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Programa de teste">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        <meta name="author" content="Adriano Reis">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Teste Adriano Reis</title>
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/style_externo.css')}}">

        <!- BOOTSTRAP ->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('bootstrap/css/bootstrap.css')}}">

         <!- JQuery ->
        <script  src="{{URL::asset('jquery/jquery-3.3.1.min.js')}}"></script>

        <!- FONTS AWESOME ->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

        <!- GOOGLE FONTS ->
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700&display=swap" rel="stylesheet"> 

        <!- icon ->
        <link rel="shortcut icon" href="{{URL::asset('img/logo-ceert-favicon.jpg')}}" class="img-circle img-responsive" style="border-radius: 10px;" type="image/png">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">


        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    </head>

<script>
    $(document).ready(function () {
                $('#linha_tempo').DataTable({
                   // "order": [[ 0, "desc", 3, "desc",]],
                    "order": [[0, "asc", 2, "des"]],
                   "bFilter": true, //mostra o filtro
                   "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
                    "bDeferRender": true,
                    "sPaginationType": "full_numbers",
                    "language": {
                        "lengthMenu": "Mostrando _MENU_ registros por página",
                        "sZeroRecords": "Não foi encontrado registros",
                        "sEmptyTable": "Não existem ítens nessa lista de compras",
                        "sInfo": "Mostrando (_START_ de _END_), de um total de _TOTAL_, registros",
                        "sInfoEmpty": "Mostrando 0 de 0 de um total de 0 registros",
                        "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Filtrar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Por favor espere - carregando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Próximo",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                   
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend: 'csv',
                            title: 'compras-do-ano',
                            text: '<i class = "fa fa-table"> </i> .csv',
                            className: 'csv'

                        },
                        {
                            extend: 'excel',
                            title: 'compras-do-ano',
                            text: '<i class = "fa fa-table"> </i> Excel',
                            className: 'excel'

                        },
                        {
                            extend: 'print',
                            title: 'compras-do-ano',
                            text: '<i class = "fa fa-print"> </i> Imprimir',
                            className: 'print'

                        },
                        {
                            extend: 'pdf',
                            title: 'compras-do-ano',
                            text: '<i class = "fa fa-file"> </i> PDF',
                            className: 'pdf'

                        },
                        {
                            extend: 'copy',
                            title: 'compras-do-ano',
                            text: '<i class = "fa fa-file"> </i> copy',
                            className: 'copy'

                        }
                        //'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                    
                });
            });
        </script>
        <body>
        <!-- MENU -->
        <section id="cima-nav-index">
            <div class="container-fluid">
                <div class="top-nav">
                    <a href="\" class="logo-menu"><img src="img/akna.jpeg" style="width: 100px;" class="round"></a>
                    <div style="display: none;" class="menu-horizontal">
                    </div>
                </div>
            </div>
        </section>

        <!-- CORPO -->
        <section id="corpo">
            <div class="container-fluid">
            <div class="row p-2">
                    <div class="col">
                        @if (Session::has('errorMsg'))
                            <div class="alert alert-danger" role="alert">
                                <span>{{ Session::get('errorMsg') }}</span>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col">
                            @if (Session::has('successMsg'))
                                <div class="alert alert-success" role="alert">
                                    <span>{{ Session::get('successMsg') }}</span>
                                </div>
                            @endif
                        </div>
                    <div class="conteudo-principal">
                         <a href="{{('\lista_de_compras') }}" target="__blank"><h1>LISTA DE COMPRAS</h1></a>

               
                <form id="frm-upload" action="{{('\importacaodados')}}" method="POST" enctype="multipart/form-data" style="margin-bottom: 80px;">
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <label for="dados" class="form-label">Selecione o arquivo de dados (CSV)</label>
                            <input class="form-control form-control-lg" name="dados" id="dados" type="file" accept=".csv" required />
                        </div>
                        <div class="col-md-6">
                            <button type="submit" style="margin-top: 34px;" class="btn btn-success" >Enviar</button>
                        </div>
                    </div>
                       
                </form>
          
                    <div class="col-md-12"></div>
                         <table class="table" id="linha_tempo">
                            <thead>
                                <tr>
                                <th scope="col" style="display:none"></th>
                                <th scope="col">Mês</th>
                                <th scope="col" style="display:none"></th>
                                <th scope="col">Categoria</th>
                                <th scope="col">Produto</th>
                                <th scope="col">Quantidade</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dados as $dado)
                                <tr>
                                <th scope="row"  style="display:none">{{$dado->id_mes}}</th>
                                <td>{{$dado->descricao}}</td>
                                <th scope="row"  style="display:none">{{$dado->categoria}}</th>
                                <td>{{$dado->descricao_cat}}</td>
                                <td>{{$dado->produto}}</td>
                                <td>{{$dado->quantidade}}</td>
                                </tr>
                                @endforeach()
                            </tbody>
                            </table>

                    </div>
                </div>
    </section>
</body>
</html>
